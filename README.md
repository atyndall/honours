Honours Project
=======

Files relating to my honours project.

Read more about it at [my blog](http://ash.id.au/tags/#honours).

Compiled thesis can be found at thesis/thesis.pdf.

See also [TaRL](http://github.com/atyndall/tarl), the Thermal Array Library that powers the thesis.
